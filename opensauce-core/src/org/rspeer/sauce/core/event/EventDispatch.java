package org.rspeer.sauce.core.event;


import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.event.Event;
import org.rspeer.runetek.event.EventListener;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public final class EventDispatch {

    private static final Set<EventListener> REGISTERED_LISTENERS = new HashSet<>();

    public static <T extends EventListener> T registerListener(T listener) {
        synchronized (REGISTERED_LISTENERS) {
            if (REGISTERED_LISTENERS.add(listener)) {
                Game.getEventDispatcher().register(listener);
                return listener;
            }

            throw new IllegalStateException("Listener is already registered.");
        }
    }

    public static <T extends EventListener> T registerListener(@SuppressWarnings("unused") Class<T> listenerType, T eventListener) {
        return registerListener(eventListener);
    }

    public static <T extends EventListener> void deregisterListener(T eventListener) {
        synchronized (REGISTERED_LISTENERS) {
            if (REGISTERED_LISTENERS.remove(eventListener)) {
                Game.getEventDispatcher().deregister(eventListener);
            }
        }
    }

    public static void deregisterListeners(Collection<EventListener> listeners) {
        listeners.forEach(EventDispatch::deregisterListener);
    }

    public static void deregisterListeners(EventListener[] listeners) {
        for (EventListener listener : listeners) {
            deregisterListener(listener);
        }
    }

    public static void deregisterAllListeners() {
        synchronized (REGISTERED_LISTENERS) {
            Iterator<EventListener> listenerator = REGISTERED_LISTENERS.iterator();
            while (listenerator.hasNext()) {
                EventListener next = listenerator.next();
                Game.getEventDispatcher().deregister(next);
                listenerator.remove();
            }
        }
    }

    public static <T extends Event<?>> void immediate(T event) {
        Game.getEventDispatcher().immediate(event);
    }

    public static <T extends Event<?>> void later(T event) {
        Game.getEventDispatcher().delay(event);
    }

}