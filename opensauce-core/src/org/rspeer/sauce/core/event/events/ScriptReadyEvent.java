package org.rspeer.sauce.core.event.events;

import org.rspeer.runetek.event.Event;
import org.rspeer.runetek.event.EventListener;
import org.rspeer.sauce.core.event.listener.ScriptReadyListener;
import org.rspeer.sauce.core.script.SauceScript;

public final class ScriptReadyEvent extends Event<SauceScript> {

    public ScriptReadyEvent(SauceScript source) {
        super(source);
    }

    @Override
    public void forward(EventListener eventListener) {
        if (eventListener instanceof ScriptReadyListener) {
            ((ScriptReadyListener) eventListener).notify(this);
        }
    }
}
