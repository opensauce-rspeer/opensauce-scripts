package org.rspeer.sauce.core.event.listener;

import org.rspeer.runetek.event.EventListener;
import org.rspeer.sauce.core.event.events.ScriptReadyEvent;

public interface ScriptReadyListener extends EventListener {

    void notify(ScriptReadyEvent event);

}
