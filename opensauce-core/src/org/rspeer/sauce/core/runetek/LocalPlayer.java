package org.rspeer.sauce.core.runetek;

import org.rspeer.runetek.adapter.scene.PathingEntity;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.local.Health;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;

public final class LocalPlayer {

    private LocalPlayer() {
        //NOP
    }

    public static Player instance() {
        return Players.getLocal();
    }

    public static boolean isInteractingWith(PathingEntity<?, ?> entity) {
        Player local = LocalPlayer.instance();
        PathingEntity<?, ?> target = local.getTarget();
        return entity == target || entity.equals(target);
    }

    public static boolean isAnimating() {
        return LocalPlayer.instance().isAnimating();
    }

    public static boolean isMoving() {
        return LocalPlayer.instance().isMoving();
    }

    public static boolean isBusy() {
        return LocalPlayer.isAnimating() || LocalPlayer.isMoving();
    }

    public static boolean isLocal(Player player) {
        Player local = LocalPlayer.instance();
        return local == player || local.equals(player);
    }

    public static Position getDestination() {
        return Movement.getDestination();
    }

    public static double getDestinationDistance() {
        return Movement.getDestinationDistance();
    }

    public static int getRunEnergy() {
        return Movement.getRunEnergy();
    }

    public static int getWeight() {
        return Movement.getPlayerWeight();
    }

    public static double getHealthPercent() {
        return Health.getPercent();
    }

    public static Position getPosition() {
        return LocalPlayer.instance().getPosition();
    }

    public static double distanceTo(BankLocation bankLocation) {
        Position bankPosition = bankLocation.getPosition();
        return bankPosition.distance();
    }

}