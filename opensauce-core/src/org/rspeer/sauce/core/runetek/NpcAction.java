package org.rspeer.sauce.core.runetek;

import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;

import java.util.Arrays;
import java.util.function.BooleanSupplier;

public final class NpcAction {

    public static Result execute(Position npcPosition, NpcAction action) {
        return execute(npcPosition, action, null, -1);
    }

    public static Result execute(Position npcPosition, NpcAction action, BooleanSupplier condition, long sleepUntil) {
        Npc npc = action.findNpc();
        if (npc == null || npc.distance() > 5 || !npc.isPositionInteractable()) {
            World.walkTo(npcPosition);
            return Result.MOVEMENT;
        }

        if (action.execute()) {
            if (condition != null) {
                Time.sleepUntil(condition, sleepUntil);
            }

            return Result.NPC_ACTION;
        }

        return Result.FAIL;
    }

    public enum Result {
        MOVEMENT, NPC_ACTION, FAIL
    }

    private final String npcName;
    private final String interactOption;
    private final Chat dialog;

    public NpcAction(String npcName, String interactOption) {
        this(npcName, interactOption, null);
    }

    public NpcAction(String npcName, String interactOption, Chat dialog) {
        this.npcName = npcName;
        this.interactOption = interactOption;
        this.dialog = dialog;
    }

    public boolean execute() {
        Npc npc = findNpc();
        if (npc == null) {
            return false;
        }

        if (dialog != null) {
            if (dialog.isActive()) {
                dialog.execute();
                return true;
            }
        }

        return npc.interact(interactOption);
    }

    private Npc findNpc() {
        return Npcs.getNearest(npc -> {
            String name = npc.getName();
            if ((name == null && npcName == null) || (name != null && name.equalsIgnoreCase(npcName))) {
                return npc.containsAction(interactOption);
            }

            return false;
        });
    }

    @Override
    public String toString() {
        return "NpcAction{" +
                "npcName='" + npcName + '\'' +
                ", interactOption='" + interactOption + '\'' +
                ", dialog=" + dialog +
                '}';
    }

    public static final class Chat {

        public static final Chat CLICK_CONTINUE = new Chat(true);

        private final boolean clickContinue;
        private final String[] chatOptions;

        public Chat(boolean clickContinue, String... chatOptions) {
            this.clickContinue = clickContinue;
            this.chatOptions = chatOptions;
        }

        public void execute() {
            if (clickContinue && Dialog.canContinue()) {
                Dialog.processContinue();
            } else if (Dialog.isOpen()) {
                Dialog.process(chatOptions);
            }
        }

        public boolean isActive() {
            return (clickContinue && Dialog.canContinue()) || Dialog.isOpen() || Dialog.isProcessing();
        }

        @Override
        public String toString() {
            return "Chat{" +
                    "clickContinue=" + clickContinue +
                    ", chatOptions=" + Arrays.toString(chatOptions) +
                    '}';
        }
    }
}