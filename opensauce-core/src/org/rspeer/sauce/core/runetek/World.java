package org.rspeer.sauce.core.runetek;

import org.rspeer.runetek.adapter.Positionable;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.commons.Range;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.Scene;

import java.util.function.BooleanSupplier;

public final class World {

    private static final Range RUN_ENERGY_RANGE = Range.of(25, 49);

    public static void walkTo(BankLocation bankLocation) {
        World.walkTo(bankLocation, null);
    }

    public static void walkTo(BankLocation bankLocation, BooleanSupplier sleepUntil) {
        Position bankLocationPosition = bankLocation.getPosition();
        World.walkTo(bankLocationPosition, sleepUntil);
    }

    public static void walkTo(Positionable positionable) {
        walkTo(positionable, 0);
    }

    public static void walkTo(Positionable positionable, int deviation) {
        World.walkTo(positionable, deviation, null);
    }

    public static void walkTo(Positionable positionable, BooleanSupplier sleepUntil) {
        walkTo(positionable, 0, sleepUntil);
    }

    public static void walkTo(Positionable positionable, int deviation, BooleanSupplier sleepUntil) {
        Position position = positionable.getPosition();
        if (position.isLoaded()) {
            Position best = Scene.findUnblocked(position);
            World.walkTo(best, deviation, sleepUntil);
            return;
        }

        World.walkTo(position, deviation, sleepUntil);
    }

    public static void walkTo(Position destination) {
        World.walkTo(destination, 0);
    }

    public static void walkTo(Position destination, int deviation) {
        World.walkTo(destination, deviation, null);
    }

    public static void walkTo(Position destination, BooleanSupplier sleepUntil) {
        World.walkTo(destination, 0, sleepUntil);
    }

    public static void walkTo(Position destination, int deviation, BooleanSupplier sleepUntil) {
        if (destination.distance() <= 2) {
            return;
        }

        Position currentDestination = LocalPlayer.getDestination();
        double destinationDistance = LocalPlayer.getDestinationDistance();

        if (currentDestination != null && currentDestination.distance(destination) <= 3) {
            return;
        }

        if (currentDestination == null || (destinationDistance <= 9 && destinationDistance > 3) || !Movement.isWalkable(currentDestination, false)) {
            if (!Movement.isRunEnabled()) {
                int localPlayerRunEnergy = LocalPlayer.getRunEnergy();
                if (localPlayerRunEnergy > World.RUN_ENERGY_RANGE.random()) {
                    Movement.toggleRun(true);
                    Time.sleepUntil(Movement::isRunEnabled, 3500);
                    return;
                }
            }

            Position localPosition = LocalPlayer.getPosition();
            if (deviation <= 0 && Movement.walkTo(destination) || Movement.walkTo(destination.randomize(deviation))) {
                Time.sleepUntil(sleepUntil == null ? new MovementSleepCondition(localPosition) : sleepUntil, 25, 2100);
            }
        }
    }

    private static class MovementSleepCondition implements BooleanSupplier {

        private final Position startPosition;

        MovementSleepCondition(Position startPosition) {
            this.startPosition = startPosition;
        }

        @Override
        public boolean getAsBoolean() {
            Player local = Players.getLocal();

            if (local.isMoving() && Movement.getDestination() != null) {
                return true;
            }

            Position currentPosition = local.getPosition();
            if (!currentPosition.equals(startPosition)) {
                return true;
            }

            int cursorState = Game.getClient().getCursorState();
            return cursorState == 3;
        }
    }

    private World() {
        // NOP
    }

}
