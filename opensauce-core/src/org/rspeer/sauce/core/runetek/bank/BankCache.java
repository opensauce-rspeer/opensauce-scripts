package org.rspeer.sauce.core.runetek.bank;

import org.rspeer.runetek.api.Definitions;
import org.rspeer.runetek.event.types.BankLoadEvent;
import org.rspeer.runetek.providers.RSItemDefinition;
import org.rspeer.runetek.providers.RSItemTable;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

public final class BankCache {

    private static final Map<Integer, Integer> BANK_ITEM_CACHE = new HashMap<>();

    private static boolean cachedItems = false;

    public static boolean isCacheValid() {
        return cachedItems;
    }

    public static boolean containsAny(String... itemName) {
        return getCount(itemName) > 0;
    }

    public static boolean containsAny(int... itemId) {
        return getCount(itemId) > 0;
    }

    public static boolean contains(Predicate<RSItemDefinition> itemPredicate) {
        return getCount(itemPredicate) > 0;
    }

    public static boolean contains(String itemName) {
        return getCount(itemName) > 0;
    }

    public static boolean contains(int itemId) {
        return getCount(itemId) > 0;
    }

    public static int getCount(Predicate<RSItemDefinition> itemPredicate) {
        int count = 0;

        synchronized (BANK_ITEM_CACHE) {
            Set<Map.Entry<Integer, Integer>> entries = BANK_ITEM_CACHE.entrySet();
            for (Map.Entry<Integer, Integer> entry : entries) {
                int itemId = entry.getKey();
                int amount = entry.getValue();

                RSItemDefinition itemDef = Definitions.getItem(itemId);
                if (itemDef != null && itemPredicate.test(itemDef)) {
                    count += amount;
                }
            }
        }

        return count;
    }

    public static int getCount(String... itemName) {
        return getCount(false, itemName);
    }

    public static int getCount(boolean contains, String... itemName) {
        return getCount(itemDef -> {
            String defName = itemDef.getName().toLowerCase();
            for (String n : itemName) {
                n = n.toLowerCase();
                if ((contains && defName.contains(n)) || defName.equals(n)) {
                    return true;
                }
            }

            return false;
        });
    }

    public static int getCount(int... itemId) {
        int count = 0;

        synchronized (BANK_ITEM_CACHE) {
            for (int i : itemId) {
                count += BANK_ITEM_CACHE.getOrDefault(i, 0);
            }
        }

        return count;
    }

    private static void updateCache(RSItemTable table) {
        int[] ids = table.getIds();
        int[] stacks = table.getStackSizes();

        synchronized (BANK_ITEM_CACHE) {
            BANK_ITEM_CACHE.clear();
            for (int i = 0; i < ids.length; i++) {
                BANK_ITEM_CACHE.put(ids[i], stacks[i]);
            }

            cachedItems = true;
        }
    }

    public static void onBankLoadEvent(BankLoadEvent event) {
        RSItemTable table = event.getBankItemTable();
        updateCache(table);
    }

}
