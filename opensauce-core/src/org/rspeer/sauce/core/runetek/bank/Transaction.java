package org.rspeer.sauce.core.runetek.bank;

import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;

import java.util.function.BooleanSupplier;
import java.util.function.Predicate;

public abstract class Transaction {

    public static final int AMOUNT_ALL = -1;
    public static final int AMOUNT_ALL_BUT_ONE = -2;

    protected final int amount;
    protected final Predicate<Item> target;
    protected final BooleanSupplier preCheck;

    protected Transaction(int amount, Predicate<Item> target, BooleanSupplier preCheck) {
        this.amount = amount;
        this.target = target;
        this.preCheck = preCheck;
    }

    public abstract Result execute();

    public enum Result {
        INSUFFICIENT_ITEMS, SUCCESSFUL, BANK_NOT_OPEN, FAILED;

        public boolean succeeded() {
            return this == SUCCESSFUL;
        }

        public boolean failed() {
            return !succeeded();
        }
    }

    public static final class Withdraw extends Transaction {

        private final Bank.WithdrawMode targetMode;

        public Withdraw(int amount, Predicate<Item> target) {
            this(amount, target, () -> false);
        }

        public Withdraw(int amount, Predicate<Item> target, BooleanSupplier preCheck) {
            this(Bank.WithdrawMode.ITEM, amount, target, preCheck);
        }

        public Withdraw(Bank.WithdrawMode mode, int amount, Predicate<Item> target) {
            this(mode, amount, target, () -> false);
        }

        public Withdraw(Bank.WithdrawMode mode, int amount, Predicate<Item> target, BooleanSupplier preCheck) {
            super(amount, target, preCheck);
            this.targetMode = mode;
        }

        @Override
        public Result execute() {
            if (!Bank.isOpen()) {
                return Result.BANK_NOT_OPEN;
            }

            if (preCheck.getAsBoolean()) {
                return Result.SUCCESSFUL;
            }

            int bankCount = Bank.getCount(target);
            if (bankCount <= 0 || (amount != AMOUNT_ALL && bankCount < amount)) {
                return Result.INSUFFICIENT_ITEMS;
            }

            Bank.WithdrawMode bankWithdrawMode = Bank.getWithdrawMode();
            if (bankWithdrawMode != targetMode) {
                Bank.setWithdrawMode(targetMode);
                if (!Time.sleepUntil(() -> Bank.getWithdrawMode() == targetMode, 150, 3100)) {
                    return Result.FAILED;
                }
            }

            int inventoryCount = Inventory.getCount(true, target);
            if (!doWithdraw() || !Time.sleepUntil(() -> Inventory.getCount(true, target) > inventoryCount, 210, 3100)) {
                return Result.FAILED;
            }

            return Result.SUCCESSFUL;
        }

        private boolean doWithdraw() {
            switch (amount) {
                case AMOUNT_ALL_BUT_ONE:
                    throw new IllegalArgumentException("Transaction amount ALL_BUT_ONE not currently supported.");

                case AMOUNT_ALL:
                    return Bank.withdrawAll(target);

                default:
                    return Bank.withdraw(target, amount);
            }
        }
    }

    public static final class Deposit extends Transaction {

        public Deposit(int amount, Predicate<Item> target) {
            this(amount, target, () -> false);
        }

        public Deposit(int amount, Predicate<Item> target, BooleanSupplier preCheck) {
            super(amount, target, preCheck);
        }

        @Override
        public Result execute() {
            if (!Bank.isOpen()) {
                return Result.BANK_NOT_OPEN;
            }

            if (preCheck.getAsBoolean()) {
                return Result.SUCCESSFUL;
            }

            int inventoryCount = Inventory.getCount(true, target);
            if (inventoryCount <= 0) {
                return Result.SUCCESSFUL;
            }

            if (!doDeposit() || !Time.sleepUntil(() -> Inventory.getCount(true, target) < inventoryCount, 210, 3100)) {
                return Result.FAILED;
            }

            return Result.SUCCESSFUL;
        }

        private boolean doDeposit() {
            switch (amount) {
                case AMOUNT_ALL_BUT_ONE:
                    throw new IllegalArgumentException("Transaction amount AMOUNT_ALL_BUT_ONE is not supported on deposit.");

                case AMOUNT_ALL:
                    return Bank.depositAll(target);

                default:
                    return Bank.deposit(target, amount);
            }
        }
    }

}
