package org.rspeer.sauce.core.script;

import org.rspeer.runetek.event.listeners.BankLoadListener;
import org.rspeer.sauce.core.event.EventDispatch;
import org.rspeer.sauce.core.event.events.ScriptReadyEvent;
import org.rspeer.sauce.core.runetek.bank.BankCache;
import org.rspeer.script.Script;
import org.rspeer.ui.Log;

public abstract class SauceScript extends Script {

    private static SauceScript instance;
    private static final int MAX_SEQUENTIAL_EXCEPTION = 5;

    private int exceptionCount = 0;

    public SauceScript() {
        SauceScript.instance = this;
    }

    @Override
    public final void onStart() {
        super.onStart();

        boolean startScript = false;

        try {
            startScript = onInit();
        } catch (Exception e) {
            Log.severe("Failed to start script.");
            Log.severe(e);
        }

        if (!startScript) {
            this.setStopping(true);
            return;
        }

        EventDispatch.registerListener(BankLoadListener.class, BankCache::onBankLoadEvent);
        EventDispatch.immediate(new ScriptReadyEvent(this));
    }

    public boolean onInit() {
        return true;
    }

    public final int loop() {
        int timeout = 100;
        try {
            timeout = onLoop();
        } catch (Exception e) {
            Log.severe(e);

            if (++exceptionCount > MAX_SEQUENTIAL_EXCEPTION) {
                Log.severe("Exceeded MAX_SEQUENTIAL_EXCEPTION count. Stopping script.");
                setStopping(true);
            }
        }

        if (timeout < -1) {
            timeout = -1;
        } else {
            timeout = Math.max(100, timeout);
        }

        return timeout;
    }

    public abstract int onLoop();

    public final void onStop() {
        super.onStop();

        try {
            EventDispatch.deregisterAllListeners();
        } catch (Exception ignore) {
            // NOP
        }

        try {
            onDispose();
        } catch (Exception e) {
            Log.severe(e);
        }
    }

    public void onDispose() {

    }

    public static <T extends SauceScript> T getInstance(Class<T> script) {
        return getInstance();
    }

    @SuppressWarnings("unchecked")
    public static <T extends SauceScript> T getInstance() {
        return (T) instance;
    }

}
