package org.rspeer.sauce.core.script.task;

import java.util.function.BooleanSupplier;
import java.util.function.IntSupplier;
import java.util.function.Supplier;

public abstract class Task extends org.rspeer.script.task.Task {

    public static SimpleTaskBuilder create() {
        return new SimpleTaskBuilder();
    }

    public static Task create(String status, BooleanSupplier validate, Runnable onValidation) {
        return create(() -> status, validate, onValidation);
    }

    public static Task create(Supplier<String> status, BooleanSupplier validate, Runnable onValidation) {
        return create().withStatus(status).validateWith(validate).onValidation(onValidation).build();
    }

    public static Task create(String status, BooleanSupplier validate, IntSupplier onValidation) {
        return create(() -> status, validate, onValidation);
    }

    public static Task create(Supplier<String> status, BooleanSupplier validate, IntSupplier onValidation) {
        return create().withStatus(status).validateWith(validate).onValidation(onValidation).build();
    }

    public abstract String getStatus();

    public static class SimpleTaskBuilder {

        private BooleanSupplier validate;
        private IntSupplier execute;
        private Supplier<String> status;

        protected SimpleTaskBuilder() {
            // NOP
        }

        public SimpleTaskBuilder validateWith(BooleanSupplier validate) {
            this.validate = validate;
            return this;
        }

        public SimpleTaskBuilder onValidation(IntSupplier onValidation) {
            this.execute = onValidation;
            return this;
        }

        public SimpleTaskBuilder onValidation(Runnable onValidation) {
            return onValidation(() -> {
                onValidation.run();
                return 100;
            });
        }

        public SimpleTaskBuilder withStatus(String status) {
            return withStatus(() -> status);
        }

        public SimpleTaskBuilder withStatus(Supplier<String> status) {
            this.status = status;
            return this;
        }

        public Task build() {
            if (execute == null) {
                throw new IllegalArgumentException("Execute function cannot be null.");
            }

            return new Task() {
                @Override
                public String getStatus() {
                    return status == null ? "Executing anonymous task." : status.get();
                }

                @Override
                public boolean validate() {
                    return validate == null || validate.getAsBoolean();
                }

                @Override
                public int execute() {
                    return execute.getAsInt();
                }
            };
        }
    }

}
