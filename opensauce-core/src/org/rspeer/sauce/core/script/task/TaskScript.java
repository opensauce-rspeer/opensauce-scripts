package org.rspeer.sauce.core.script.task;

import org.rspeer.sauce.core.script.SauceScript;

public abstract class TaskScript extends SauceScript {

    private final TaskSet taskSetProxy = new TaskSet() {
        @Override
        public boolean validate() {
            return true;
        }
    };

    protected void register(Task... task) {
        taskSetProxy.register(task);
    }

    @Override
    public final int onLoop() {
        return taskSetProxy.execute();
    }

    public String getStatus() {
        return taskSetProxy.getStatus();
    }

}
