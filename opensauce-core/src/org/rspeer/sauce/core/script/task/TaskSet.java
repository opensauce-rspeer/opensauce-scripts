package org.rspeer.sauce.core.script.task;

import org.rspeer.runetek.event.EventListener;
import org.rspeer.sauce.core.event.EventDispatch;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public abstract class TaskSet extends Task {

    private boolean sortedTasks = false;
    private Task currentTask = null;

    private final List<Task> tasks = new LinkedList<>();

    public void register(Task... task) {
        sortedTasks = false;

        synchronized (tasks) {
            for (Task t : task) {
                tasks.add(t);
                if (t instanceof EventListener) {
                    EventDispatch.registerListener((EventListener) t);
                }
            }
        }
    }

    @Override
    public int execute() {

        Task validTask = null;

        synchronized (tasks) {
            if (!sortedTasks) {
                sortTasks();
            }

            for (Task t : tasks) {
                if (t.validate()) {
                    currentTask = validTask = t;
                    break;
                }
            }
        }

        if (validTask == null) {
            return 100;
        }

        return validTask.execute();
    }

    private void sortTasks() {
        tasks.sort(Comparator.comparing(org.rspeer.script.task.Task::getPriority));
        sortedTasks = true;
    }

    @Override
    public String getStatus() {
        Task task = currentTask;
        if (task == null) {
            return "Waiting";
        }

        String taskStatus = task.getStatus();
        if (taskStatus == null || taskStatus.isEmpty()) {
            return "Executing " + task.getClass().getSimpleName();
        }

        return taskStatus;
    }
}
