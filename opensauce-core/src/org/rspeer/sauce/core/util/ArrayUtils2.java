package org.rspeer.sauce.core.util;

import java.util.function.BooleanSupplier;

public final class ArrayUtils2 {

    public static boolean allMatch(BooleanSupplier... supplier) {
        for (BooleanSupplier sup : supplier) {
            if (!sup.getAsBoolean()) {
                return false;
            }
        }

        return true;
    }

    public static boolean anyMatch(BooleanSupplier... supplier) {
        for (BooleanSupplier sup : supplier) {
            if (sup.getAsBoolean()) {
                return true;
            }
        }

        return false;
    }

    private ArrayUtils2() {
        // NOP
    }

}
