package org.rspeer.sauce.script.questr;

import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.sauce.core.event.EventDispatch;
import org.rspeer.sauce.core.script.task.TaskScript;
import org.rspeer.sauce.script.questr.core.QuestExecutor;
import org.rspeer.sauce.script.questr.core.task.*;
import org.rspeer.sauce.script.questr.quests.cooksassitant.CooksAssistant;
import org.rspeer.sauce.script.questr.quests.romeoandjuliet.RomeoAndJuliet;
import org.rspeer.sauce.script.questr.quests.runemysteries.RuneMysteries;
import org.rspeer.script.ScriptCategory;
import org.rspeer.script.ScriptMeta;
import org.rspeer.ui.Log;

import java.awt.*;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

@ScriptMeta(developer = "balsamic", name = "OpenSauce Quest'r", version = 0.1, desc = "An open-sourced questing script.", category = ScriptCategory.QUESTING)
public final class QuestScript extends TaskScript {

    private QuestExecutor currentQuestExecutor = null;
    private Queue<QuestExecutor> questQueue = new LinkedBlockingQueue<>();

    @Override
    public boolean onInit() {
        register(
                new DequeueQuest(),
                new CacheBank(),
                new ExecuteQuest(),
                new QuestCompletionScreen(),
                new ClickContinue()
        );

        questQueue.add(new RuneMysteries());
        questQueue.add(new CooksAssistant());
        questQueue.add(new RomeoAndJuliet());

        EventDispatch.registerListener(RenderListener.class, event -> {
            Graphics g = event.getSource();
            String status = getStatus().concat("...");

            g.setColor(Color.BLACK);
            g.drawString(status, 16, 37);

            g.setColor(Color.RED);
            g.drawString(status, 16, 36);
        });

        return true;
    }

    public void setCurrentQuestExecutor(QuestExecutor currentQuestExecutor) {
        if (this.currentQuestExecutor != null) {
            try {
                this.currentQuestExecutor.dispose();
            } catch (Exception e) {
                Log.severe("An error occurred when disposing of previous quest executor.");
                Log.severe(e);
            }
        }

        currentQuestExecutor.init();
        this.currentQuestExecutor = currentQuestExecutor;
    }

    public QuestExecutor getCurrentQuestExecutor() {
        return currentQuestExecutor;
    }

    public Queue<QuestExecutor> getQuestQueue() {
        return questQueue;
    }
}
