package org.rspeer.sauce.script.questr.core;

import org.rspeer.runetek.api.Varps;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.sauce.core.util.ArrayUtils2;
import org.rspeer.sauce.script.questr.core.requirement.IRequirement;

public enum Quest {

    //F2P
    @QuestMeta(freeToPlay = true, awardedQuestPoints = 1, awardedItems = {@QuestMeta.ItemReward(itemId = 1438)}, miscRewards = {"Access to essence mines", "Access to Runecrafting skill"})
    RUNE_MYSTERIES("Rune Mysteries", 63, 6),
    @QuestMeta(freeToPlay = true, awardedQuestPoints = 1, awardedExperience = {@QuestMeta.ExperienceReward(skill = Skill.COOKING, amount = 300)}, miscRewards = "Ability to use range in Lumbridge castle.")
    COOKS_ASSISTANT("Cook's Assistant", 29, 2),
    @QuestMeta(freeToPlay = true, awardedQuestPoints = 5)
    ROMEO_AND_JULIET("Romeo & Juliet", 144, 100)
    //P2P
    ;

    private final int varpId;
    private final int stages;
    private final String name;
    private IRequirement[] requirements;

    Quest(String name, int varpId, int stages) {
        this(name, varpId, stages, IRequirement.NONE);
    }

    Quest(String name, int varpId, int stages, IRequirement[] requirements) {
        this.name = name;
        this.varpId = varpId;
        this.stages = stages;
        this.requirements = requirements;
    }

    public int getVarpId() {
        return varpId;
    }

    public int getStage() {
        return Varps.get(varpId);
    }

    public String getName() {
        return name;
    }

    public boolean isNotStarted() {
        return getStage() == 0;
    }

    public boolean isStarted() {
        return getStage() > 0;
    }

    public boolean isComplete() {
        return getStage() >= stages;
    }

    public IRequirement[] getRequirements() {
        return requirements;
    }

    public boolean isRequirementsMet() {
        IRequirement[] requirements = getRequirements();
        if (requirements.length == 0) {
            return true;
        }

        return ArrayUtils2.allMatch(requirements);
    }

    public QuestMeta getMeta() {
        String enumName = name();

        try {
            return Quest.class.getField(enumName).getAnnotation(QuestMeta.class);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }
}
