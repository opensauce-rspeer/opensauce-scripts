package org.rspeer.sauce.script.questr.core;

import org.rspeer.runetek.event.EventListener;
import org.rspeer.sauce.core.event.EventDispatch;
import org.rspeer.ui.Log;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public abstract class QuestExecutor {

    private StageHandler currentStage;
    private final Set<EventListener> registeredListeners = Collections.synchronizedSet(new LinkedHashSet<>());
    private final Set<StageHandler> stages = Collections.synchronizedSet(new LinkedHashSet<>());

    public final void init() {
        this.onInit();
    }

    public abstract void onInit();

    public int execute() {
        StageHandler nextHandler = nextStageHandler();
        if (nextHandler == null) {
            if (!getQuest().isComplete()) {
                throw new IllegalStateException("No handler for quest [" + getQuest().getName() + "; stage: " + getQuest().getStage() + "].");
            }

            return -1;
        }

        currentStage = nextHandler;
        return nextHandler.execute();
    }

    public final void dispose() {
        EventDispatch.deregisterListeners(registeredListeners);

        synchronized (stages) {
            Iterator<StageHandler> iterator = stages.iterator();
            while (iterator.hasNext()) {
                StageHandler next = iterator.next();
                iterator.remove();

                try {
                    next.dispose();
                } catch (Exception ignore) {
                    // NOP
                }
            }
        }

        try {
            onDispose();
        } catch (Exception e) {
            Log.severe(e);
        }
    }

    public void onDispose() {
        // NOP
    }

    private StageHandler nextStageHandler() {
        synchronized (stages) {
            for (StageHandler handler : stages) {
                if (handler.canExecute()) {
                    return handler;
                }
            }
        }

        return null;
    }

    protected void register(StageHandler... stage) {
        for (StageHandler s : stage) {
            if (!stages.add(s)) {
                throw new IllegalStateException("Failed to register stage [" + s.getClass().getSimpleName() + "].");
            }

            s.setExecutor(this);
            s.init();
        }
    }

    public abstract Quest getQuest();

    public boolean cacheBank() {
        return false;
    }

    public String getStatus() {
        return currentStage == null ? "Waiting" : currentStage.getStatus();
    }

}
