package org.rspeer.sauce.script.questr.core;

import org.rspeer.runetek.api.component.tab.Skill;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface QuestMeta {

    boolean freeToPlay();

    int awardedQuestPoints();

    ItemReward[] awardedItems() default {};

    ExperienceReward[] awardedExperience() default {};

    String[] miscRewards() default {};

    @interface ItemReward {

        int itemId();

        int amount() default 1;

    }

    @interface ExperienceReward {

        Skill skill();

        int amount();

    }

}
