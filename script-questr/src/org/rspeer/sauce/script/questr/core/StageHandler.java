package org.rspeer.sauce.script.questr.core;

import org.rspeer.runetek.api.commons.ArrayUtils;

public abstract class StageHandler {

    private QuestExecutor executor;

    void init() {
        this.onInit();
    }

    public void onInit() {
        // NOP
    }

    void setExecutor(QuestExecutor executor) {
        if (this.executor != null) {
            throw new IllegalStateException("An executor has already been assigned to this handler.");
        }

        this.executor = executor;
    }

    public boolean canExecute() {
        int questStage = getQuest().getStage();
        int[] supportedStages = getSupportedStageIds();
        return ArrayUtils.contains(supportedStages, questStage);
    }

    public abstract int execute();

    void dispose() {
        this.onDispose();
    }

    public void onDispose() {
        // NOP
    }

    public abstract int[] getSupportedStageIds();

    public abstract String getStatus();

    protected Quest getQuest() {
        if (this.executor == null) {
            throw new IllegalStateException("An executor is not assigned to this handler.");
        }

        return this.executor.getQuest();
    }

}
