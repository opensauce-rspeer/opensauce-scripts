package org.rspeer.sauce.script.questr.core.requirement;

import java.util.function.BooleanSupplier;

public interface IRequirement extends BooleanSupplier {

    IRequirement[] NONE = new IRequirement[0];

    boolean isMet();

    @Override
    default boolean getAsBoolean() {
        return isMet();
    }
}
