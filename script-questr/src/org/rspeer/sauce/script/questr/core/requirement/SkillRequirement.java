package org.rspeer.sauce.script.questr.core.requirement;

import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;

public final class SkillRequirement implements IRequirement {

    private final Skill skill;
    private final int minimumLevel;

    public SkillRequirement(Skill skill, int minimumLevel) {
        this.skill = skill;
        this.minimumLevel = minimumLevel;
    }

    @Override
    public boolean isMet() {
        return Skills.getCurrentLevel(skill) >= minimumLevel;
    }
}
