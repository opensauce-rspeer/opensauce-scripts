package org.rspeer.sauce.script.questr.core.stage;

import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.sauce.core.runetek.NpcAction;
import org.rspeer.sauce.script.questr.core.StageHandler;

import java.util.function.Supplier;

public class NpcActionHandler extends StageHandler {

    private final Position npcPosition;
    private final NpcAction npcAction;
    private Supplier<String> status;
    private final int[] supportedStages;

    public NpcActionHandler(Position npcPosition, NpcAction npcAction, Supplier<String> status, int... supportedStages) {
        this.npcAction = npcAction;
        this.npcPosition = npcPosition;
        this.status = status;
        this.supportedStages = supportedStages;
    }

    @Override
    public int execute() {
        NpcAction.Result actionResult = NpcAction.execute(npcPosition, npcAction);
        if (actionResult == NpcAction.Result.FAIL) {
            throw new IllegalStateException("Failed to execute " + npcAction);
        }

        return Random.mid(100, 800);
    }

    @Override
    public int[] getSupportedStageIds() {
        return this.supportedStages;
    }

    @Override
    public String getStatus() {
        return status.get();
    }
}
