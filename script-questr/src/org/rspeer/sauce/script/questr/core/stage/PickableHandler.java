package org.rspeer.sauce.script.questr.core.stage;

import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Pickables;
import org.rspeer.sauce.core.runetek.LocalPlayer;
import org.rspeer.sauce.core.runetek.World;
import org.rspeer.sauce.script.questr.core.StageHandler;

import java.util.function.BooleanSupplier;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class PickableHandler extends StageHandler {

    private final Predicate<Pickable> itemPredicate;
    private final BooleanSupplier shouldPick;
    private final Position itemPosition;
    private Supplier<String> status;
    private final int[] supportedStages;

    public PickableHandler(Position itemPosition, Predicate<Pickable> item, Supplier<String> status, int... supportedStages) {
        this(itemPosition, item, () -> true, status, supportedStages);
    }

    public PickableHandler(Position itemPosition, Predicate<Pickable> item, BooleanSupplier shouldPick, Supplier<String> status, int... supportedStages) {
        this.itemPredicate = item;
        this.itemPosition = itemPosition;
        this.shouldPick = shouldPick;
        this.status = status;
        this.supportedStages = supportedStages;
    }

    @Override
    public boolean canExecute() {
        return super.canExecute() && shouldPick.getAsBoolean();
    }

    @Override
    public int execute() {
        if (itemPosition.distance() > 5) {
            World.walkTo(itemPosition);
            return Random.mid(200, 900);
        }

        int count = Inventory.getCount(true);
        Pickable pickable = Pickables.getNearest(itemPredicate);
        if (pickable != null && pickable.interact("Take") && Time.sleepUntil(() -> Inventory.getCount(true) > count, LocalPlayer::isMoving, 2100)) {
            return 100;
        }

        return Random.mid(300, 1200);
    }

    @Override
    public int[] getSupportedStageIds() {
        return this.supportedStages;
    }

    @Override
    public String getStatus() {
        return status.get();
    }
}
