package org.rspeer.sauce.script.questr.core.stage;

import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.sauce.core.runetek.LocalPlayer;
import org.rspeer.sauce.core.runetek.World;
import org.rspeer.sauce.script.questr.core.StageHandler;

import java.util.function.BooleanSupplier;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class SceneObjectHandler extends StageHandler {

    private final Position objectPosition;
    private final Predicate<SceneObject> sceneObject;
    private String interactAction;
    private final BooleanSupplier shouldInteract;
    private final BooleanSupplier timeoutCondition;
    private final int timeoutDuration;
    private final Supplier<String> status;
    private final int[] supportedStages;

    public SceneObjectHandler(Position objectPosition, Predicate<SceneObject> sceneObject, String interactAction, BooleanSupplier shouldInteract, BooleanSupplier timeoutCondition, int timeoutDuration, Supplier<String> status, int... supportedStages) {
        this.objectPosition = objectPosition;
        this.sceneObject = sceneObject;
        this.interactAction = interactAction;
        this.shouldInteract = shouldInteract;
        this.timeoutCondition = timeoutCondition;
        this.timeoutDuration = timeoutDuration;
        this.status = status;
        this.supportedStages = supportedStages;
    }

    @Override
    public boolean canExecute() {
        return super.canExecute() && shouldInteract.getAsBoolean();
    }

    @Override
    public int execute() {
        if (objectPosition.distance() > 5) {
            World.walkTo(objectPosition);
            return Random.mid(200, 900);
        }

        SceneObject target = SceneObjects.getNearest(sceneObject);
        if (target == null) {
            throw new IllegalStateException("Target object is null.");
        }

        if (!target.isPositionInteractable()) {
            World.walkTo(target.getPosition());
            return Random.mid(200, 900);
        }

        if (target.interact(interactAction)) {
            if (Time.sleepUntil(timeoutCondition, LocalPlayer::isMoving, timeoutDuration)) {
                return 100;
            }
        }

        return Random.mid(300, 1200);
    }

    @Override
    public int[] getSupportedStageIds() {
        return this.supportedStages;
    }

    @Override
    public String getStatus() {
        return status.get();
    }
}
