package org.rspeer.sauce.script.questr.core.stage;

import org.rspeer.sauce.core.script.task.Task;
import org.rspeer.sauce.core.script.task.TaskSet;
import org.rspeer.sauce.script.questr.core.StageHandler;
import org.rspeer.script.task.Condition;

public abstract class TaskSetHandler extends StageHandler implements Condition {

    private final TaskSet taskSetProxy = new TaskSet() {
        @Override
        public boolean validate() {
            return true;
        }
    };

    public void register(Task... task) {
        taskSetProxy.register(task);
    }

    public abstract boolean validate();

    @Override
    public final boolean canExecute() {
        return super.canExecute() && validate();
    }

    @Override
    public final int execute() {
        return taskSetProxy.execute();
    }

    @Override
    public String getStatus() {
        return taskSetProxy.getStatus();
    }
}
