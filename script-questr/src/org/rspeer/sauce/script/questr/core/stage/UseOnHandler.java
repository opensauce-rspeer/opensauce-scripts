package org.rspeer.sauce.script.questr.core.stage;

import org.rspeer.runetek.adapter.Interactable;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Identifiable;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.sauce.core.runetek.LocalPlayer;
import org.rspeer.sauce.core.runetek.World;
import org.rspeer.sauce.script.questr.core.StageHandler;

import java.util.function.BooleanSupplier;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class UseOnHandler<T extends Interactable & Identifiable> extends StageHandler {

    private final Predicate<Item> targetItem;
    private final Predicate<T> targetInteractable;
    private final Position objectPosition;
    private Class<T> interactableType;
    private BooleanSupplier useValidate;
    private int useTimeout;
    private Supplier<String> status;
    private final int[] supportedStages;

    public UseOnHandler(Position position, Predicate<Item> targetItem, Predicate<T> targetInteractable, Class<T> interactableType, BooleanSupplier useValidate, int useTimeout, Supplier<String> status, int... supportedStages) {
        this.targetItem = targetItem;
        this.targetInteractable = targetInteractable;
        this.objectPosition = position;
        this.interactableType = interactableType;
        this.useValidate = useValidate;
        this.useTimeout = useTimeout;
        this.status = status;
        this.supportedStages = supportedStages;
    }

    @Override
    public int execute() {
        if (objectPosition.distance() > 5) {
            World.walkTo(objectPosition);
            return Random.mid(200, 900);
        }

        if (!Inventory.contains(targetItem)) {
            throw new IllegalStateException("Target item is not found in inventory.");
        }

        T interactable = findInteractable();
        if (Inventory.use(targetItem, interactable)) {
            Time.sleepUntil(useValidate, LocalPlayer::isMoving, useTimeout);
        }

        return 100;
    }

    @Override
    public int[] getSupportedStageIds() {
        return this.supportedStages;
    }

    @Override
    public String getStatus() {
        return status.get();
    }

    @SuppressWarnings("unchecked")
    private T findInteractable() {
        if (interactableType == SceneObject.class) {
            return (T) SceneObjects.getNearest(obj -> targetInteractable.test((T) obj));
        }

        if (interactableType == Npc.class) {
            return (T) Npcs.getNearest(npc -> targetInteractable.test((T) npc));
        }

        throw new IllegalArgumentException("Unsupported target interactable type. [" + interactableType.getSimpleName() + "].");
    }
}
