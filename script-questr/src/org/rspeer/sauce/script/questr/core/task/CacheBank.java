package org.rspeer.sauce.script.questr.core.task;

import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.sauce.core.runetek.LocalPlayer;
import org.rspeer.sauce.core.runetek.World;
import org.rspeer.sauce.core.runetek.bank.BankCache;
import org.rspeer.sauce.core.script.SauceScript;
import org.rspeer.sauce.core.script.task.Task;
import org.rspeer.sauce.script.questr.QuestScript;
import org.rspeer.sauce.script.questr.core.QuestExecutor;

public final class CacheBank extends Task {

    private String status = "Caching bank items";

    @Override
    public boolean validate() {
        QuestExecutor questExecutor = SauceScript.getInstance(QuestScript.class).getCurrentQuestExecutor();
        return !BankCache.isCacheValid() && questExecutor != null && questExecutor.cacheBank();
    }

    @Override
    public int execute() {
        if (Bank.isOpen()) { // Bank was open before listener was registered. Close bank.
            status = "Closing bank";
            Bank.close();
            Time.sleepUntil(Bank::isClosed, 3100);
            return 100;
        }

        BankLocation nearest = BankLocation.getNearest(bankLocation -> bankLocation.getType() != BankLocation.Type.DEPOSIT_BOX);
        if (nearest == null) {
            throw new IllegalStateException("Unable to locate nearest bank.");
        }

        if (LocalPlayer.distanceTo(nearest) > 5) {
            status = "Walking to bank";
            World.walkTo(nearest);
            return Random.mid(200, 900);
        }

        if (!Bank.isOpen()) {
            status = "Opening bank";
            if (!Bank.open(nearest) || !Time.sleepUntil(BankCache::isCacheValid, 300, 3100)) {
                return 100;
            }
        }

        status = "Closing bank";
        Bank.close();
        return Random.mid(400, 2100);
    }

    @Override
    public String getStatus() {
        return status;
    }
}
