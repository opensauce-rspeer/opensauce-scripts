package org.rspeer.sauce.script.questr.core.task;

import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.sauce.core.script.task.Task;

public final class ClickContinue extends Task {

    @Override
    public String getStatus() {
        return "Clicking continue";
    }

    @Override
    public boolean validate() {
        return Dialog.canContinue();
    }

    @Override
    public int execute() {
        if (Dialog.processContinue()) {
            Time.sleepUntil(() -> !Dialog.canContinue() || Dialog.isProcessing(), 2100);
        }

        return 100;
    }
}
