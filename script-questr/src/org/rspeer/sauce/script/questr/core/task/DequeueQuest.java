package org.rspeer.sauce.script.questr.core.task;

import org.rspeer.sauce.core.script.SauceScript;
import org.rspeer.sauce.core.script.task.Task;
import org.rspeer.sauce.script.questr.QuestScript;
import org.rspeer.sauce.script.questr.core.QuestExecutor;
import org.rspeer.ui.Log;

import java.util.Queue;

public final class DequeueQuest extends Task {

    @Override
    public boolean validate() {
        QuestScript questScript = SauceScript.getInstance(QuestScript.class);
        return questScript.getCurrentQuestExecutor() == null || questScript.getCurrentQuestExecutor().getQuest().isComplete();
    }

    @Override
    public int execute() {
        Queue<QuestExecutor> questQueue = SauceScript.getInstance(QuestScript.class).getQuestQueue();
        if (questQueue.isEmpty()) {
            Log.info("No more quests available. Stopping script.");
            return -1;
        }

        QuestExecutor next = questQueue.poll();
        SauceScript.getInstance(QuestScript.class).setCurrentQuestExecutor(next);

        return 100;
    }

    @Override
    public String getStatus() {
        return "De-queuing next quest";
    }
}
