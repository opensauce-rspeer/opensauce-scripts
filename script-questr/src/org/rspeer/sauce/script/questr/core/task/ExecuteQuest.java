package org.rspeer.sauce.script.questr.core.task;

import org.rspeer.sauce.core.script.SauceScript;
import org.rspeer.sauce.core.script.task.Task;
import org.rspeer.sauce.script.questr.QuestScript;
import org.rspeer.sauce.script.questr.core.QuestExecutor;

public final class ExecuteQuest extends Task {

    @Override
    public boolean validate() {
        QuestExecutor questExecutor = SauceScript.getInstance(QuestScript.class).getCurrentQuestExecutor();
        return questExecutor != null;
    }

    @Override
    public int execute() {
        QuestExecutor questExecutor = SauceScript.getInstance(QuestScript.class).getCurrentQuestExecutor();
        if (questExecutor == null) {
            throw new IllegalStateException("Quest executor is null.");
        }

        return questExecutor.execute();
    }

    @Override
    public String getStatus() {
        QuestExecutor questExecutor = SauceScript.getInstance(QuestScript.class).getCurrentQuestExecutor();
        return questExecutor == null ? "Waiting" : questExecutor.getStatus();
    }
}
