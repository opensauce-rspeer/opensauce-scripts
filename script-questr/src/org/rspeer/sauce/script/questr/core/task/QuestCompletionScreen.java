package org.rspeer.sauce.script.questr.core.task;

import org.rspeer.runetek.adapter.component.InterfaceComponent;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.InterfaceAddress;
import org.rspeer.runetek.api.component.InterfaceComposite;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.sauce.core.script.task.Task;

public final class QuestCompletionScreen extends Task {


    private static final InterfaceAddress IFACE_ADDRESS_QUEST_COMPLETION_SCREEN = new InterfaceAddress(InterfaceComposite.QUEST_COMPLETED);

    @Override
    public String getStatus() {
        return "Closing quest completion screen";
    }

    @Override
    public boolean validate() {
        return Interfaces.isOpen(IFACE_ADDRESS_QUEST_COMPLETION_SCREEN);
    }

    @Override
    public int execute() {
        if (Interfaces.closeAll()) {
            return 100;
        }

        InterfaceComponent closeButton = IFACE_ADDRESS_QUEST_COMPLETION_SCREEN.component(16).resolve();
        if (closeButton != null && closeButton.click()) {
            Time.sleepUntil(() -> Interfaces.isOpen(IFACE_ADDRESS_QUEST_COMPLETION_SCREEN), 3500);
        }

        return 100;
    }
}
