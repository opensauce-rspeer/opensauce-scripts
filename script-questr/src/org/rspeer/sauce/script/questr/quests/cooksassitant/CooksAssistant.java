package org.rspeer.sauce.script.questr.quests.cooksassitant;

import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.predicate.IdPredicate;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.sauce.core.runetek.NpcAction;
import org.rspeer.sauce.script.questr.core.Quest;
import org.rspeer.sauce.script.questr.core.QuestExecutor;
import org.rspeer.sauce.script.questr.core.stage.NpcActionHandler;
import org.rspeer.sauce.script.questr.core.stage.PickableHandler;
import org.rspeer.sauce.script.questr.core.stage.UseOnHandler;
import org.rspeer.sauce.script.questr.quests.cooksassitant.stages.FetchBankedItems;
import org.rspeer.sauce.script.questr.quests.cooksassitant.stages.MillFlour;

public final class CooksAssistant extends QuestExecutor {

    public static final int ITEM_ID_BUCKET = 1925;
    public static final int ITEM_ID_BUCKET_OF_MILK = 1927;
    public static final int ITEM_ID_POT = 1931;
    public static final int ITEM_ID_POT_OF_FLOUR = 1933;
    public static final int ITEM_ID_EGG = 1944;
    public static final int ITEM_ID_GRAIN = 1947;

    public static final String OBJECT_NAME_DAIRY_COW = "Dairy cow";
    public static final String OBJECT_NAME_WHEAT = "Wheat";
    public static final String OBJECT_NAME_HOPPER = "Hopper";
    public static final String OBJECT_NAME_MILL_CONTROLS = "Hopper controls";
    public static final String OBJECT_NAME_FLOUR_BIN = "Flour bin";

    public static final int ANIMATION_ID_MILL_CONTROLS = 3571;

    public static final Position POSITION_COOK = new Position(3207, 3213, 0);
    public static final Position POSITION_DAIRY_COW = new Position(3255, 3273, 0);
    public static final Position POSITION_CHICKEN_PEN = new Position(3230, 3298, 0);
    public static final Position POSITION_BUCKET = new Position(3215, 9624, 0);
    public static final Position POSITION_WHEAT = new Position(3160, 3296, 0);
    public static final Position POSITION_HOPPER = new Position(3166, 3308, 2);
    public static final Position POSITION_MILL_CONTROLS = new Position(3165, 3305, 2);
    public static final Position POSITION_FLOUR_BIN = new Position(3167, 3305, 0);

    @Override
    public void onInit() {
        register(
                new FetchBankedItems(),
                new NpcActionHandler(
                        POSITION_COOK,
                        new NpcAction("Cook", "Talk-to",
                                new NpcAction.Chat(true, "What's wrong?", "I'm always happy to help a cook in distress.", "Actually, I know where to find this stuff.")
                        ), () -> "Starting quest", 0
                ),
                new PickableHandler(POSITION_COOK, new IdPredicate<>(ITEM_ID_POT), () -> !hasPot() && !hasPotOfFlour(), () -> "Taking Pot", 1),
                new PickableHandler(POSITION_BUCKET, new IdPredicate<>(ITEM_ID_BUCKET), () -> !hasBucket() && !hasBucketOfMilk(), () -> "Taking Bucket", 1),
                new UseOnHandler<SceneObject>(POSITION_DAIRY_COW, new IdPredicate<>(ITEM_ID_BUCKET), sceneObject -> sceneObject.getName().equalsIgnoreCase(OBJECT_NAME_DAIRY_COW), SceneObject.class, () -> Inventory.contains(ITEM_ID_BUCKET_OF_MILK), 5100, () -> "Milking Cow", 1) {
                    @Override
                    public boolean canExecute() {
                        return super.canExecute() && !Inventory.contains(ITEM_ID_BUCKET_OF_MILK);
                    }
                },
                new PickableHandler(POSITION_CHICKEN_PEN, new IdPredicate<>(ITEM_ID_EGG), () -> !hasEgg(), () -> "Taking Egg", 1),
                new MillFlour(),
                new NpcActionHandler(POSITION_COOK, new NpcAction("Cook", "Talk-to", NpcAction.Chat.CLICK_CONTINUE), () -> "Completing quest", 1)
        );
    }

    @Override
    public Quest getQuest() {
        return Quest.COOKS_ASSISTANT;
    }

    @Override
    public boolean cacheBank() {
        return true;
    }

    public static boolean hasBucket() {
        return Inventory.contains(ITEM_ID_BUCKET);
    }

    public static boolean hasBucketOfMilk() {
        return Inventory.contains(ITEM_ID_BUCKET_OF_MILK);
    }

    public static boolean hasEgg() {
        return Inventory.contains(ITEM_ID_EGG);
    }

    public static boolean hasPot() {
        return Inventory.contains(ITEM_ID_POT);
    }

    public static boolean hasPotOfFlour() {
        return Inventory.contains(ITEM_ID_POT_OF_FLOUR);
    }
}
