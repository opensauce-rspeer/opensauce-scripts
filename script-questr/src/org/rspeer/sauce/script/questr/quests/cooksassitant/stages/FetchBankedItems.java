package org.rspeer.sauce.script.questr.quests.cooksassitant.stages;

import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.commons.predicate.IdPredicate;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.sauce.core.runetek.LocalPlayer;
import org.rspeer.sauce.core.runetek.World;
import org.rspeer.sauce.core.runetek.bank.BankCache;
import org.rspeer.sauce.core.runetek.bank.Transaction;
import org.rspeer.sauce.script.questr.core.StageHandler;
import org.rspeer.sauce.script.questr.quests.cooksassitant.CooksAssistant;

public final class FetchBankedItems extends StageHandler {

    @Override
    public boolean canExecute() {
        if (!super.canExecute()) {
            return false;
        }

        if (!CooksAssistant.hasPotOfFlour() && !CooksAssistant.hasPot()) {
            return BankCache.containsAny(CooksAssistant.ITEM_ID_POT, CooksAssistant.ITEM_ID_POT_OF_FLOUR);
        }

        if (!CooksAssistant.hasBucket() && !CooksAssistant.hasBucketOfMilk()) {
            return BankCache.containsAny(CooksAssistant.ITEM_ID_BUCKET, CooksAssistant.ITEM_ID_BUCKET_OF_MILK);
        }

        return !CooksAssistant.hasEgg() && BankCache.contains(CooksAssistant.ITEM_ID_EGG);
    }

    @Override
    public int execute() {
        if (!Bank.isOpen()) {
            if (LocalPlayer.distanceTo(BankLocation.LUMBRIDGE_CASTLE) > 5) {
                World.walkTo(BankLocation.LUMBRIDGE_CASTLE);
                return Random.mid(200, 900);
            }

            if (!Bank.open(BankLocation.LUMBRIDGE_CASTLE) || !Time.sleepUntil(Bank::isOpen, 3100)) {
                return 800;
            }

            return 100;
        }

        Transaction.Withdraw bucketOfMilk = new Transaction.Withdraw(1, new IdPredicate<>(CooksAssistant.ITEM_ID_BUCKET_OF_MILK), () -> Inventory.contains(CooksAssistant.ITEM_ID_BUCKET_OF_MILK));
        if (bucketOfMilk.execute().failed()) {
            Transaction.Withdraw bucket = new Transaction.Withdraw(1, new IdPredicate<>(CooksAssistant.ITEM_ID_BUCKET), () -> Inventory.contains(CooksAssistant.ITEM_ID_BUCKET));
            bucket.execute();
        }

        Transaction.Withdraw potOfFlour = new Transaction.Withdraw(1, new IdPredicate<>(CooksAssistant.ITEM_ID_POT_OF_FLOUR), () -> Inventory.contains(CooksAssistant.ITEM_ID_POT_OF_FLOUR));
        if (potOfFlour.execute().failed()) {
            Transaction.Withdraw pot = new Transaction.Withdraw(1, new IdPredicate<>(CooksAssistant.ITEM_ID_POT), () -> Inventory.contains(CooksAssistant.ITEM_ID_POT));
            pot.execute();
        }

        Transaction.Withdraw egg = new Transaction.Withdraw(1, new IdPredicate<>(CooksAssistant.ITEM_ID_EGG), () -> Inventory.contains(CooksAssistant.ITEM_ID_EGG));
        egg.execute();

        if (Bank.isOpen() && Bank.close()) {
            Time.sleepUntil(Bank::isClosed, 2100);
        }

        return 100;
    }

    @Override
    public int[] getSupportedStageIds() {
        return new int[]{0, 1};
    }

    @Override
    public String getStatus() {
        return "Withdrawing required items from bank";
    }
}
