package org.rspeer.sauce.script.questr.quests.cooksassitant.stages;

import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.commons.predicate.IdPredicate;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.sauce.core.runetek.LocalPlayer;
import org.rspeer.sauce.core.runetek.World;
import org.rspeer.sauce.core.script.task.Task;
import org.rspeer.sauce.script.questr.core.stage.TaskSetHandler;
import org.rspeer.sauce.script.questr.quests.cooksassitant.CooksAssistant;

public final class MillFlour extends TaskSetHandler {

    private State state = State.RETRIEVE_FLOUR;

    @Override
    public void onInit() {
        register(
                Task.create("Thinking", () -> state == State.INDETERMINATE, this::determineState),
                Task.create("Picking wheat", () -> state == State.PICK_WHEAT, this::pickWheat),
                Task.create("Adding wheat to hopper", () -> state == State.ADD_TO_HOPPER, this::fillHopper),
                Task.create("Operating mill", () -> state == State.MILL_CONTROLS, this::operateMill),
                Task.create("Retrieving milled flour", () -> state == State.RETRIEVE_FLOUR, this::retrieveFlour)
        );
    }

    @Override
    public boolean validate() {
        return !CooksAssistant.hasPotOfFlour();
    }

    @Override
    public int[] getSupportedStageIds() {
        return new int[]{1};
    }

    private void determineState() {
        if (Inventory.contains(CooksAssistant.ITEM_ID_GRAIN)) {
            state = State.ADD_TO_HOPPER;
        } else {
            state = State.PICK_WHEAT;
        }
    }

    private int pickWheat() {
        if (CooksAssistant.POSITION_WHEAT.distance() > 5) {
            World.walkTo(CooksAssistant.POSITION_WHEAT);
            return Random.mid(200, 900);
        }

        SceneObject wheatObject = SceneObjects.getNearest(CooksAssistant.OBJECT_NAME_WHEAT);
        if (wheatObject != null && wheatObject.interact("Pick")) {
            if (Time.sleepUntil(() -> Inventory.contains(CooksAssistant.ITEM_ID_GRAIN), LocalPlayer::isMoving, 3100)) {
                state = State.ADD_TO_HOPPER;
                return 100;
            }
        }

        return Random.mid(400, 2100);
    }

    private int fillHopper() {
        if (CooksAssistant.POSITION_HOPPER.distance() > 4) {
            World.walkTo(CooksAssistant.POSITION_HOPPER);
            return Random.mid(200, 900);
        }

        SceneObject hopper = SceneObjects.getNearest(CooksAssistant.OBJECT_NAME_HOPPER);
        if (hopper == null) {
            throw new IllegalStateException("Grain hopper is null.");
        }

        if (Inventory.use(new IdPredicate<>(CooksAssistant.ITEM_ID_GRAIN), hopper) && Time.sleepUntil(() -> !Inventory.contains(CooksAssistant.ITEM_ID_GRAIN), 3100)) {
            state = State.MILL_CONTROLS;
            return 100;
        }

        return Random.mid(400, 2100);
    }

    private int operateMill() {
        if (CooksAssistant.POSITION_MILL_CONTROLS.distance() > 3) {
            World.walkTo(CooksAssistant.POSITION_MILL_CONTROLS);
            return Random.mid(200, 900);
        }

        SceneObject hopperControls = SceneObjects.getNearest(CooksAssistant.OBJECT_NAME_MILL_CONTROLS);
        if (hopperControls == null) {
            throw new IllegalStateException("Hopper controls is null.");
        }

        if (hopperControls.interact("Operate")) {
            if (Time.sleepUntil(LocalPlayer::isAnimating, LocalPlayer::isMoving, 25, 3100)) {
                state = State.RETRIEVE_FLOUR;
                return 100;
            }
        }

        return Random.mid(400, 2100);
    }

    private int retrieveFlour() {
        if (CooksAssistant.POSITION_FLOUR_BIN.distance() > 4 || LocalPlayer.getPosition().getFloorLevel() != CooksAssistant.POSITION_FLOUR_BIN.getFloorLevel()) { //weird daxwalker bug with floor level
            World.walkTo(CooksAssistant.POSITION_FLOUR_BIN);
            return Random.mid(200, 900);
        }

        SceneObject flourBin = SceneObjects.getNearest(CooksAssistant.OBJECT_NAME_FLOUR_BIN);
        if (flourBin == null) {
            throw new IllegalStateException("Flour bin is null.");
        }

        if (flourBin.interact("Empty") && Time.sleepUntil(() -> Inventory.contains(CooksAssistant.ITEM_ID_POT_OF_FLOUR), LocalPlayer::isMoving, 3100)) {
            state = State.PICK_WHEAT;
            return 100;
        }

        return Random.mid(200, 900);
    }

    private enum State {
        INDETERMINATE,
        PICK_WHEAT,
        ADD_TO_HOPPER,
        MILL_CONTROLS,
        RETRIEVE_FLOUR
    }

}
