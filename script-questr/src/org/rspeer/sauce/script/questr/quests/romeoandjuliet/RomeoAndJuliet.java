package org.rspeer.sauce.script.questr.quests.romeoandjuliet;

import org.rspeer.runetek.api.commons.predicate.NamePredicate;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.sauce.core.runetek.LocalPlayer;
import org.rspeer.sauce.core.runetek.NpcAction;
import org.rspeer.sauce.script.questr.core.Quest;
import org.rspeer.sauce.script.questr.core.QuestExecutor;
import org.rspeer.sauce.script.questr.core.stage.NpcActionHandler;
import org.rspeer.sauce.script.questr.core.stage.SceneObjectHandler;

public final class RomeoAndJuliet extends QuestExecutor {

    public static final int ITEM_ID_CADAVA_BERRIES = 753;
    public static final int ITEM_ID_CADAVA_POTION = 756;

    public static final Position POSITION_ROMEO = new Position(3209, 3425, 0);
    public static final Position POSITION_JULIET = new Position(3158, 3425, 1);
    public static final Position POSITION_FATHER_LAWRENCE = new Position(3254, 3481, 0);
    public static final Position POSITION_APOTHECARIST = new Position(3194, 3402, 0);
    public static final Position POSITION_CADVA_BUSH = new Position(3269, 3368, 0);

    public static final Position POSITION_JULIET_CUTSCENE = new Position(12045, 457, 1);
    public static final Position POSITION_ROMEO_CUTSCENE = new Position(11469, 846, 0);

    @Override
    public void onInit() {
        NpcActionHandler romeo = new NpcActionHandler(
                POSITION_ROMEO,
                new NpcAction(
                        "Romeo", "Talk-to",
                        new NpcAction.Chat(true,
                                "Perhaps I could help to find her for you?", "Yes, ok, I'll let her know.", "Ok, thanks.")
                ), () -> "Talking to Romeo", 0, 20, 60
        );

        NpcActionHandler juliet = new NpcActionHandler(
                POSITION_JULIET,
                new NpcAction("Juliet", "Talk-to", NpcAction.Chat.CLICK_CONTINUE),
                () -> "Talking to Juliet", 10
        ) {
            @Override
            public boolean canExecute() {
                return super.canExecute() || (Inventory.contains(ITEM_ID_CADAVA_POTION) && Quest.ROMEO_AND_JULIET.getStage() == 50) || LocalPlayer.getPosition().equals(POSITION_JULIET_CUTSCENE);
            }
        };

        NpcActionHandler fatherLawrence = new NpcActionHandler(
                POSITION_FATHER_LAWRENCE,
                new NpcAction("Father Lawrence", "Talk-to", NpcAction.Chat.CLICK_CONTINUE),
                () -> "Talking to Father Lawrence", 30
        );

        NpcActionHandler apothecarist = new NpcActionHandler(
                POSITION_APOTHECARIST,
                new NpcAction("Apothecary", "Talk-to",
                        new NpcAction.Chat(true, "Talk about something else.", "Talk about Romeo & Juliet.", "Ok,thanks.")
                ), () -> "Talking to Apothecary", 40
        ) {
            @Override
            public boolean canExecute() {
                return super.canExecute() || (Quest.ROMEO_AND_JULIET.getStage() == 50 && Inventory.contains(ITEM_ID_CADAVA_BERRIES));
            }
        };

        SceneObjectHandler cadavaBush = new SceneObjectHandler(
                POSITION_CADVA_BUSH, new NamePredicate<>(false, "Cadava bush"),
                "Pick-from", () -> !Inventory.contains(ITEM_ID_CADAVA_BERRIES),
                () -> LocalPlayer.isAnimating() || Inventory.contains(ITEM_ID_CADAVA_BERRIES), 3100,
                () -> "Picking Cadava berries", 50
        ) {
            @Override
            public boolean canExecute() {
                return super.canExecute() && !Inventory.contains(ITEM_ID_CADAVA_POTION);
            }
        };

        register(romeo, juliet, fatherLawrence, apothecarist, cadavaBush);
    }

    @Override
    public Quest getQuest() {
        return Quest.ROMEO_AND_JULIET;
    }
}
