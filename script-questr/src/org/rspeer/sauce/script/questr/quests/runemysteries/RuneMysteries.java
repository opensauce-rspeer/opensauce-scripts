package org.rspeer.sauce.script.questr.quests.runemysteries;

import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.sauce.core.runetek.NpcAction;
import org.rspeer.sauce.script.questr.core.Quest;
import org.rspeer.sauce.script.questr.core.QuestExecutor;
import org.rspeer.sauce.script.questr.core.stage.NpcActionHandler;

public final class RuneMysteries extends QuestExecutor {

    @Override
    public void onInit() {
        NpcActionHandler dukeHoracio = new NpcActionHandler(
                new Position(3210, 3222, 1),
                new NpcAction("Duke Horacio", "Talk-to", new NpcAction.Chat(true, "Have you any quests for me?", "Sure, no problem.")),
                () -> "Taking to Duke Horacio", 0
        );

        NpcActionHandler sedridor = new NpcActionHandler(
                new Position(3105, 9571, 0),
                new NpcAction("Sedridor", "Talk-to", new NpcAction.Chat(true, "I'm looking for the head wizard.", "Ok, here you are.", "Yes, certainly.")),
                () -> "Talking to Sedridor", 1, 2, 5
        );

        NpcActionHandler aubury = new NpcActionHandler(
                new Position(3252, 3401, 0),
                new NpcAction("Aubury", "Talk-to", new NpcAction.Chat(true, "I have been sent here with a package for you.")),
                () -> "Talking to Aubury", 3, 4
        );

        register(dukeHoracio, sedridor, aubury);
    }

    @Override
    public Quest getQuest() {
        return Quest.RUNE_MYSTERIES;
    }
}
